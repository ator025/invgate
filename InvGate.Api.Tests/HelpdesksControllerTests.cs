﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvGate.Api.Controllers;
using InvGate.Api.Dtos;
using InvGate.Api.Infrastructure;
using InvGate.Api.Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InvGate.Api.Tests
{
    [TestClass]
    public class HelpdesksControllerTests
    {
        private Mock<IInvGateApiService> _invGateApiServiceMock;
        private AnalyticsContext _analiticsContextInMemory;
        private Mock<TextSearchRankRepository> _textSearchRankRepositoryMock;
        private Mock<ILogger<HelpdesksController>> _loggerMock;

        public HelpdesksControllerTests()
        {
            _invGateApiServiceMock = new Mock<IInvGateApiService>();
            // creando una base de datos en memoria para unit tests.
            _analiticsContextInMemory = new AnalyticsContext(new DbContextOptionsBuilder<AnalyticsContext>()
                                                            .UseInMemoryDatabase(databaseName: "unit_testing_database")
                                                            .Options);
            _textSearchRankRepositoryMock = new Mock<TextSearchRankRepository>(_analiticsContextInMemory){CallBase=true};
            _loggerMock = new Mock<ILogger<HelpdesksController>>();
        }

        [TestMethod]
        public async Task GetAsync_detailed_response_success()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId = "11";

            _invGateApiServiceMock.Setup(api => api.GetIncidentsByHelpdeskIdAsync(It.IsAny<string>()))
                                .ReturnsAsync(new List<string>(){"expected_1","expected_2","expected_3"});
            var incidentDtos = new List<IncidentDto>(){
                new IncidentDto(){ id= "expected_1", assigned_id="assigned_1", assigned_group_id="group_1", description=textToSearch},
                new IncidentDto(){ id= "expected_2", assigned_id="assigned_2", assigned_group_id="group_2", description=textToSearch},
                new IncidentDto(){ id= "expected_3", assigned_id="assigned_3", assigned_group_id="group_3", description=textToSearch},
                };
            _invGateApiServiceMock.Setup(api => api.GetFullIncidentsByDescriptionAsync(It.IsAny<IEnumerable<string>>(), It.Is<string>(s=> s == textToSearch)))
                                .ReturnsAsync(incidentDtos);

            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync(helpdeskId, textToSearch, 1) as OkObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.OK);
            var responseDto = response.Value as HelpdeskDetailedResponseDto;
            Assert.IsNotNull(responseDto);
            Assert.AreEqual(3, responseDto.incidents.Count);

            Assert.AreEqual(3, responseDto.incidents.Count(i=>i.description == textToSearch));
            
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.id == "expected_1"));
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.id == "expected_2"));
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.id == "expected_3"));
            
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.assigned_id == "assigned_1"));
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.assigned_id == "assigned_2"));
            Assert.AreEqual(1, responseDto.incidents.Count(i=>i.assigned_id == "assigned_3"));
            
            // Checking DB
            var row = _analiticsContextInMemory.TextSearchRanks.First(t => t.SearchedText == textToSearch);
            Assert.AreEqual(textToSearch, row.SearchedText);
            Assert.AreEqual(helpdeskId, row.HelpdeskId);
            Assert.AreNotEqual(0, row.Timestamp);

        }

        [TestMethod]
        public async Task GetAsync_short_response_success()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId = "115";

            _invGateApiServiceMock.Setup(api => api.GetIncidentsByHelpdeskIdAsync(It.IsAny<string>()))
                                .ReturnsAsync(new List<string>(){"expected_1","expected_2","expected_3"});
            var incidentDtos = new List<IncidentDto>(){
                new IncidentDto(){ id= "expected_1", assigned_id="assigned_1", assigned_group_id="group_1", description=textToSearch},
                new IncidentDto(){ id= "expected_2", assigned_id="assigned_2", assigned_group_id="group_2", description=textToSearch},
                new IncidentDto(){ id= "expected_3", assigned_id="assigned_3", assigned_group_id="group_3", description=textToSearch},
                };
            _invGateApiServiceMock.Setup(api => api.GetFullIncidentsByDescriptionAsync(It.IsAny<IEnumerable<string>>(), It.Is<string>(s=> s == textToSearch)))
                                .ReturnsAsync(incidentDtos);

            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync(helpdeskId, textToSearch, 0) as OkObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.OK);
            var responseDto = response.Value as HelpdeskShortResponseDto;
            Assert.IsNotNull(responseDto);
            Assert.AreEqual(3, responseDto.incidentIds.Count);

            Assert.AreEqual(1, responseDto.incidentIds.Count(i=>i == "expected_1"));
            Assert.AreEqual(1, responseDto.incidentIds.Count(i=>i == "expected_2"));
            Assert.AreEqual(1, responseDto.incidentIds.Count(i=>i == "expected_3"));
            
            // Checking DB
            var row = _analiticsContextInMemory.TextSearchRanks.First(t => t.SearchedText == textToSearch);
            Assert.AreEqual(textToSearch, row.SearchedText);
            Assert.AreEqual(helpdeskId, row.HelpdeskId);
            Assert.AreNotEqual(0, row.Timestamp);

        }
        
        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow("        ")]  // cada uno de los DataRow values va a ser evaluado son 3 unit test en uno
        public async Task GetAsync_invalid_helpdeskId(string invalid_helpdesk_id)
        {
            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync(invalid_helpdesk_id, "search", 0);

            //Assert
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult)); // 400 status code --> Bad request

        }
        [DataTestMethod]
        [DataRow(2)]
        [DataRow(-1)]
        [DataRow(1525454)]  // cada uno de los DataRow values va a ser evaluado son 3 unit test en uno
        public async Task GetAsync_invalid_detailed(int invalid_detailed)
        {
            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync("11", "search", invalid_detailed);

            //Assert
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult)); // 400 status code --> Bad request

        }
        
        [TestMethod]
        public async Task GetAsync_incidents_notFound()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId = "115";

            _invGateApiServiceMock.Setup(api => api.GetIncidentsByHelpdeskIdAsync(It.IsAny<string>()))
                                .ReturnsAsync(new List<string>(){});

            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync(helpdeskId, textToSearch, 1) as NotFoundObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.NotFound);
            
            // Checking DB
            var row = _analiticsContextInMemory.TextSearchRanks.FirstOrDefault(t => t.SearchedText == textToSearch);
            Assert.IsNull(row);
            
        }

        [TestMethod]
        public async Task GetAsync_text_search_notFound()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId = "115";

            _invGateApiServiceMock.Setup(api => api.GetIncidentsByHelpdeskIdAsync(It.IsAny<string>()))
                                .ReturnsAsync(new List<string>(){"expected_1","expected_2","expected_3"});
            
            _invGateApiServiceMock.Setup(api => api.GetFullIncidentsByDescriptionAsync(It.IsAny<IEnumerable<string>>(), It.Is<string>(s=> s == textToSearch)))
                                .ReturnsAsync(new List<IncidentDto>(){});

            //Act
            var helpdeskController = new HelpdesksController(_invGateApiServiceMock.Object, _textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await helpdeskController.GetAsync(helpdeskId, textToSearch, 1) as NotFoundObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.NotFound);
            
            // Checking DB
            var row = _analiticsContextInMemory.TextSearchRanks.First(t => t.SearchedText == textToSearch);
            Assert.AreEqual(textToSearch, row.SearchedText);
            Assert.AreEqual(helpdeskId, row.HelpdeskId);
            Assert.AreNotEqual(0, row.Timestamp);

        }
    }
}
