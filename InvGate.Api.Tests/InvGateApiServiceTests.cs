using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InvGate.Api.Controllers;
using InvGate.Api.Dtos;
using InvGate.Api.Infrastructure;
using InvGate.Api.Infrastructure.Repositories;
using InvGate.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace InvGate.Api.Tests
{
    [TestClass]
    public class InvGateApiServiceTests
    {
        private Mock<HttpMessageHandler> _httpMessageHandlerMock;
        private Mock<ILogger<InvGateApiService>> _loggerMock;

        public InvGateApiServiceTests()
        {
            _httpMessageHandlerMock = new Mock<HttpMessageHandler>(){CallBase=true};
            _loggerMock = new Mock<ILogger<InvGateApiService>>();
        }

        [TestMethod]
        public async Task GetIncidentsByHelpdeskIdAsync_success()
        {
            //Arrenge
            var helpdeskId = "2";

            dynamic dynResponse = new ExpandoObject(); 
            dynResponse.status= "OK";
            dynResponse.requestIds = new List<string> (){"expected_1", "expected_2", "expected_3"};

            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(dynResponse))
                                                   });
            
            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetIncidentsByHelpdeskIdAsync(helpdeskId);

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(3, response.Count());
            
            Assert.AreEqual(1, response.Count(i=>i == "expected_1"));
            Assert.AreEqual(1, response.Count(i=>i == "expected_2"));
            Assert.AreEqual(1, response.Count(i=>i == "expected_3"));
            _httpMessageHandlerMock.Protected().Verify<Task<HttpResponseMessage>>("SendAsync", Times.Once()
                                        , ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get && r.RequestUri.AbsoluteUri == $"https://webdemo.cloud.invgate.net/api/v1/incidents.by.helpdesk?helpdesk_id={helpdeskId}")
                                        , ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        [ExpectedException(typeof(ExternalApiException))]
        public async Task GetIncidentsByHelpdeskIdAsync_notOk_throwApiException()
        {
            //Arrenge
            var helpdeskId = "2";

            dynamic dynResponse = new ExpandoObject(); 
            dynResponse.status= "NOT OK";
            dynResponse.requestIds = new List<string> (){"expected_1", "expected_2", "expected_3"};

            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(dynResponse))
                                                   });
            
            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetIncidentsByHelpdeskIdAsync(helpdeskId);

            //Assert
            Assert.Fail();
        }
        
        [TestMethod]
        [ExpectedException(typeof(ExternalApiException))]
        public async Task GetIncidentsByHelpdeskIdAsync_ExternalApiReturn500_throwApiException()
        {
            //Arrenge
            var helpdeskId = "2";

            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.InternalServerError, 
                                                   });
            
            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetIncidentsByHelpdeskIdAsync(helpdeskId);

            //Assert
            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(ExternalApiException))]
        public async Task GetIncidentsByHelpdeskIdAsync_httpClient_throwTimeoutException_throwApiException()
        {
            //Arrenge
            var helpdeskId = "2";

            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                                               .ThrowsAsync(new TimeoutException());
            
            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetIncidentsByHelpdeskIdAsync(helpdeskId);

            //Assert
            Assert.Fail();
        }

        [TestMethod]
        public async Task GetFullIncidentsByDescriptionAsync_success()
        {
            //Arrenge
            var textToSearch = "expected";
            var incidentIds = new List<string>(){"incident_1","incident_2","incident_3", "incident_4", "incident_5", "incident_6"};

            var incident_1=new IncidentDto(){ id= "incindent_1", assigned_id="assigned_1", assigned_group_id="group_1", description=Guid.NewGuid() + textToSearch};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_1")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_1), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_2=new IncidentDto(){ id= "incindent_2", assigned_id="assigned_2", assigned_group_id="group_2", description= textToSearch + Guid.NewGuid() };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_2")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_2), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_3=new IncidentDto(){ id= "incindent_3", assigned_id="assigned_3", assigned_group_id="group_3", description=$"   {Guid.NewGuid()} {textToSearch}  asd asd asd "};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_3")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_3), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_4=new IncidentDto(){ id= "incindent_4", assigned_id="assigned_4", assigned_group_id="group_4", description= textToSearch};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_4")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_4), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_5=new IncidentDto(){ id= "incindent_5", assigned_id="assigned_5", assigned_group_id="group_5", description= "expect"};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_5")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_5), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_6=new IncidentDto(){ id= "incindent_6", assigned_id="assigned_6", assigned_group_id="group_6", description= $"   {Guid.NewGuid()} expect ed"};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_6")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK,
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_6), Encoding.UTF8, "application/json")
                                                   });

            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetFullIncidentsByDescriptionAsync(incidentIds, textToSearch);

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(4, response.Count());
            
            Assert.AreEqual(1, response.Count(i=>i.id == "incindent_1" && i.description.Contains(textToSearch)));
            Assert.AreEqual(1, response.Count(i=>i.id == "incindent_2" && i.description.Contains(textToSearch)));
            Assert.AreEqual(1, response.Count(i=>i.id == "incindent_3" && i.description.Contains(textToSearch)));
            Assert.AreEqual(1, response.Count(i=>i.id == "incindent_4" && i.description.Contains(textToSearch)));
            
            _httpMessageHandlerMock.Protected().Verify<Task<HttpResponseMessage>>("SendAsync", Times.Exactly(6)
                                        , ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get && r.RequestUri.AbsoluteUri.StartsWith($"https://webdemo.cloud.invgate.net/api/v1/incident?id="))
                                        , ItExpr.IsAny<CancellationToken>());
        }


        [TestMethod]
        public async Task GetFullIncidentsByDescriptionAsync_notMatching_textSearch_success()
        {
            //Arrenge
            var textToSearch = "unexpected";
            var textToAddToDescription = "expected";
            var incidentIds = new List<string>(){"incident_1","incident_2","incident_3", "incident_4", "incident_5", "incident_6"};

            var incident_1=new IncidentDto(){ id= "incindent_1", assigned_id="assigned_1", assigned_group_id="group_1", description=Guid.NewGuid() + textToAddToDescription};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_1")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_1), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_2=new IncidentDto(){ id= "incindent_2", assigned_id="assigned_2", assigned_group_id="group_2", description= textToAddToDescription + Guid.NewGuid() };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_2")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_2), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_3=new IncidentDto(){ id= "incindent_3", assigned_id="assigned_3", assigned_group_id="group_3", description=$"   {Guid.NewGuid()} {textToAddToDescription}  asd asd asd "};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_3")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_3), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_4=new IncidentDto(){ id= "incindent_4", assigned_id="assigned_4", assigned_group_id="group_4", description= textToAddToDescription};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_4")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_4), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_5=new IncidentDto(){ id= "incindent_5", assigned_id="assigned_5", assigned_group_id="group_5", description= "expect"};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_5")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK, 
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_5), Encoding.UTF8, "application/json")
                                                   });
            
            var incident_6=new IncidentDto(){ id= "incindent_6", assigned_id="assigned_6", assigned_group_id="group_6", description= $"   {Guid.NewGuid()} expect ed"};
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(rm => rm.RequestUri.AbsoluteUri.EndsWith($"incident_6")), ItExpr.IsAny<CancellationToken>())
                                               .ReturnsAsync(new HttpResponseMessage(){
                                                   StatusCode = HttpStatusCode.OK,
                                                   Content = new StringContent(JsonConvert.SerializeObject(incident_6), Encoding.UTF8, "application/json")
                                                   });

            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetFullIncidentsByDescriptionAsync(incidentIds, textToSearch);

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Count());

            _httpMessageHandlerMock.Protected().Verify<Task<HttpResponseMessage>>("SendAsync", Times.Exactly(6)
                                        , ItExpr.Is<HttpRequestMessage>(r => r.Method == HttpMethod.Get && r.RequestUri.AbsoluteUri.StartsWith($"https://webdemo.cloud.invgate.net/api/v1/incident?id="))
                                        , ItExpr.IsAny<CancellationToken>());
        }


        [TestMethod]
        [ExpectedException(typeof(ExternalApiException))]
        public async Task GetFullIncidentsByDescriptionAsync_httpClient_throwTimeoutException_throwApiException()
        {
            //Arrenge
            var textToSearch = "unexpected";
            var incidentIds = new List<string>(){"incident_1","incident_2","incident_3", "incident_4", "incident_5", "incident_6"};

            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                                               .ThrowsAsync(new TimeoutException());
            
            //Act
            var apiService = new InvGateApiService(_httpMessageHandlerMock.Object, _loggerMock.Object);
            var response = await apiService.GetFullIncidentsByDescriptionAsync(incidentIds, textToSearch);

            //Assert
            Assert.Fail();
        }
    }
}
