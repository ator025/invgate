using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvGate.Api.Controllers;
using InvGate.Api.Dtos;
using InvGate.Api.Infrastructure;
using InvGate.Api.Infrastructure.Repositories;
using InvGate.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InvGate.Api.Tests
{
    [TestClass]
    public class AnalyticsControllerTests
    {
        private Mock<ILogger<AnalyticsController>> _loggerMock;

        public AnalyticsControllerTests()
        {
            _loggerMock = new Mock<ILogger<AnalyticsController>>();
        }

        [TestMethod]
        public async Task GetSearchTextRank_byHelpDesk_success()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId_1 = "11";
            var helpdeskId_2 = "22";
            var helpdeskId_3 = "33";
            var helpdeskId_4 = "44";
            var helpdeskId_5 = "55";
            var helpdeskId_6 = "66";

            var guid_1 = Guid.NewGuid().ToString();
            var guid_2 = Guid.NewGuid().ToString();
            var guid_3 = Guid.NewGuid().ToString();
            var guid_4 = Guid.NewGuid().ToString();
            var guid_5 = Guid.NewGuid().ToString();
            var guid_6 = Guid.NewGuid().ToString();
            
            // mocking database in memory
            var analiticsContextInMemory = new AnalyticsContext(new DbContextOptionsBuilder<AnalyticsContext>()
                                                            .UseInMemoryDatabase(databaseName: "unit_testing_database_1")
                                                            .Options);
            var textSearchRankRepositoryMock = new Mock<TextSearchRankRepository>(analiticsContextInMemory){CallBase=true};


            await GenerateTextSearch(10, guid_1, helpdeskId_1, textSearchRankRepositoryMock.Object ); // expected 1
            await GenerateTextSearch(3, guid_1, helpdeskId_2, textSearchRankRepositoryMock.Object);
            await GenerateTextSearch(5, guid_2, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 3
            await GenerateTextSearch(6, guid_3, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 2
            await GenerateTextSearch(6, guid_4, helpdeskId_3, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(4, guid_4, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 4
            await GenerateTextSearch(5, guid_6, helpdeskId_6, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(3, guid_5, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 5
            await GenerateTextSearch(1, guid_6, helpdeskId_1, textSearchRankRepositoryMock.Object); // excluded
            await GenerateTextSearch(10, guid_1, helpdeskId_4, textSearchRankRepositoryMock.Object); 

            //Act
            var analyticsController = new AnalyticsController(textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await analyticsController.GetSearchTextRank(helpdeskId_1) as OkObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.OK);
            var responseDto = response.Value as TextSearchRankResponseDto;

            Assert.IsNotNull(responseDto);
            Assert.AreEqual(5, responseDto.rank.Count);
            
            Assert.AreEqual(1, responseDto.rank[0].Rank);
            Assert.AreEqual(10, responseDto.rank[0].Count);
            Assert.IsTrue(responseDto.rank[0].Text.Equals(guid_1));

            Assert.AreEqual(2, responseDto.rank[1].Rank);
            Assert.AreEqual(6, responseDto.rank[1].Count);
            Assert.IsTrue(responseDto.rank[1].Text.Equals(guid_3));

            Assert.AreEqual(3, responseDto.rank[2].Rank);
            Assert.AreEqual(5, responseDto.rank[2].Count);
            Assert.IsTrue(responseDto.rank[2].Text.Equals(guid_2));

            Assert.AreEqual(4, responseDto.rank[3].Rank);
            Assert.AreEqual(4, responseDto.rank[3].Count);
            Assert.IsTrue(responseDto.rank[3].Text.Equals(guid_4));


            Assert.AreEqual(5, responseDto.rank[4].Rank);
            Assert.AreEqual(3, responseDto.rank[4].Count);
            Assert.IsTrue(responseDto.rank[4].Text.Equals(guid_5));

        }

        [TestMethod]
        public async Task GetSearchTextRank_Global_success()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId_1 = "11";
            var helpdeskId_2 = "22";
            var helpdeskId_3 = "33";
            var helpdeskId_4 = "44";
            var helpdeskId_5 = "55";
            var helpdeskId_6 = "66";

            var guid_1 = Guid.NewGuid().ToString(); // 11   2
            var guid_2 = Guid.NewGuid().ToString(); // 18   1
            var guid_3 = Guid.NewGuid().ToString(); // 6    4
            var guid_4 = Guid.NewGuid().ToString(); // 10   3
            var guid_5 = Guid.NewGuid().ToString(); // 3
            var guid_6 = Guid.NewGuid().ToString(); // 5    5
            
            // mocking database in memory
            var analiticsContextInMemory = new AnalyticsContext(new DbContextOptionsBuilder<AnalyticsContext>()
                                                            .UseInMemoryDatabase(databaseName: "unit_testing_database_2")
                                                            .Options);
            var textSearchRankRepositoryMock = new Mock<TextSearchRankRepository>(analiticsContextInMemory){CallBase=true};

            await GenerateTextSearch(10, guid_1, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(3, guid_2, helpdeskId_2, textSearchRankRepositoryMock.Object);
            await GenerateTextSearch(5, guid_2, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(6, guid_3, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(6, guid_4, helpdeskId_3, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(4, guid_4, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(5, guid_6, helpdeskId_6, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(3, guid_5, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(1, guid_1, helpdeskId_1, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(10, guid_2, helpdeskId_4, textSearchRankRepositoryMock.Object); 

           

            //Act
            var analyticsController = new AnalyticsController(textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await analyticsController.GetSearchTextRank("") as OkObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.OK);
            var responseDto = response.Value as TextSearchRankResponseDto;

            Assert.IsNotNull(responseDto);
            Assert.AreEqual(5, responseDto.rank.Count);
            
            Assert.AreEqual(1, responseDto.rank[0].Rank);
            Assert.AreEqual(18, responseDto.rank[0].Count);
            Assert.IsTrue(responseDto.rank[0].Text.Equals(guid_2));

            Assert.AreEqual(2, responseDto.rank[1].Rank);
            Assert.AreEqual(11, responseDto.rank[1].Count);
            Assert.IsTrue(responseDto.rank[1].Text.Equals(guid_1));

            Assert.AreEqual(3, responseDto.rank[2].Rank);
            Assert.AreEqual(10, responseDto.rank[2].Count);
            Assert.IsTrue(responseDto.rank[2].Text.Equals(guid_4));

            Assert.AreEqual(4, responseDto.rank[3].Rank);
            Assert.AreEqual(6, responseDto.rank[3].Count);
            Assert.IsTrue(responseDto.rank[3].Text.Equals(guid_3));


            Assert.AreEqual(5, responseDto.rank[4].Rank);
            Assert.AreEqual(5, responseDto.rank[4].Count);
            Assert.IsTrue(responseDto.rank[4].Text.Equals(guid_6));

        }

        [TestMethod]
        public async Task GetSearchTextRank_byHelpDesk_notFound()
        {
            //Arrenge
            var textToSearch = Guid.NewGuid().ToString();
            var helpdeskId_1 = "11";
            var helpdeskId_2 = "22";
            var helpdeskId_3 = "33";
            var helpdeskId_4 = "44";
            var helpdeskId_5 = "55";
            var helpdeskId_6 = "66";

            var guid_1 = Guid.NewGuid().ToString();
            var guid_2 = Guid.NewGuid().ToString();
            var guid_3 = Guid.NewGuid().ToString();
            var guid_4 = Guid.NewGuid().ToString();
            var guid_5 = Guid.NewGuid().ToString();
            var guid_6 = Guid.NewGuid().ToString();
            
            // mocking database in memory
            var analiticsContextInMemory = new AnalyticsContext(new DbContextOptionsBuilder<AnalyticsContext>()
                                                            .UseInMemoryDatabase(databaseName: "unit_testing_database_1")
                                                            .Options);
            var textSearchRankRepositoryMock = new Mock<TextSearchRankRepository>(analiticsContextInMemory){CallBase=true};


            await GenerateTextSearch(10, guid_1, helpdeskId_1, textSearchRankRepositoryMock.Object ); // expected 1
            await GenerateTextSearch(3, guid_1, helpdeskId_2, textSearchRankRepositoryMock.Object);
            await GenerateTextSearch(5, guid_2, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 3
            await GenerateTextSearch(6, guid_3, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 2
            await GenerateTextSearch(6, guid_4, helpdeskId_3, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(4, guid_4, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 4
            await GenerateTextSearch(5, guid_6, helpdeskId_6, textSearchRankRepositoryMock.Object); 
            await GenerateTextSearch(3, guid_5, helpdeskId_1, textSearchRankRepositoryMock.Object); // expected 5
            await GenerateTextSearch(1, guid_6, helpdeskId_1, textSearchRankRepositoryMock.Object); // excluded
            await GenerateTextSearch(10, guid_1, helpdeskId_4, textSearchRankRepositoryMock.Object); 

            //Act
            var analyticsController = new AnalyticsController(textSearchRankRepositoryMock.Object, _loggerMock.Object);
            var response = await analyticsController.GetSearchTextRank(helpdeskId_5) as NotFoundObjectResult;

            //Assert
            Assert.AreEqual(response.StatusCode, (int)System.Net.HttpStatusCode.NotFound);

        }

        private async Task GenerateTextSearch(int qty, string guid, string helpdeskId, TextSearchRankRepository repository)
        {
            for (int i = 0; i < qty; i++)
            {
                var searchText = guid;
                await repository.Create(new TextSearch(){ SearchedText = searchText, HelpdeskId = helpdeskId });
            }
        }

    }
}
