namespace InvGate.Api.Models
{
    public class TextSearchCount
    {
        public string SearchedText { get; set; }
        public long Count { get; set; }
    }
}