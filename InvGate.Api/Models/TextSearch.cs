using System.ComponentModel.DataAnnotations;

namespace InvGate.Api.Models
{
    public class TextSearch
    {
        [Key]
        public long Id { get; set; }
        public string HelpdeskId { get; set; }
        public string SearchedText { get; set; }
        public long Timestamp { get; set; }
    }
}