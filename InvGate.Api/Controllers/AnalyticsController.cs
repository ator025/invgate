﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvGate.Api.Dtos;
using InvGate.Api.Infrastructure;
using InvGate.Api.Infrastructure.Repositories;
using InvGate.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace InvGate.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnalyticsController : ControllerBase
    {
        private readonly ITextSearchRankRepository textSearchRankRepository;
        private readonly ILogger<AnalyticsController> logger;

        public AnalyticsController(ITextSearchRankRepository textSearchRankRepository, ILogger<AnalyticsController> logger)
        {
            this.textSearchRankRepository = textSearchRankRepository;
            this.logger = logger;
        }

        /// <summary>
        /// Endpoint para traer el ranking de las palabras mas buscadas por helpdesk
        /// ATENCION!! Se usa una base de datos IN MEMORY por lo que cada vez que se inicia la app el ranking va a estar vacio!
        /// </summary>
        /// <remarks>
        /// SAMPLE:
        ///     GET http://localhost:5010/api/analytics/searchtextrank?helpdesk_id=2
        /// </remarks>
        /// <param name="helpdesk_Id"> Especifica el helpdesk_id para buscar los 5 textos más buscados. Si no se manda parámetro helpdesk_id, devuelve los 5 textos más buscados globalmente.</param>
        /// <response code="200">Regresa el ranking que fue solicitado</response>
        /// <response code="404">Cuando no se encuentra informacion para armar el ranking</response>     
        [HttpGet]
        [Route("searchtextrank")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Produces("application/json")]
        public async Task<IActionResult> GetSearchTextRank([FromQuery] string helpdesk_Id)
        {
            var queryResult = new List<TextSearchCount>();
            if (string.IsNullOrWhiteSpace(helpdesk_Id))
            {
                queryResult = await textSearchRankRepository.GetGlobalTop5TextSearchesOrderByCountDescAsync();
            }
            else
            {
                queryResult = await textSearchRankRepository.GetTop5TextSearchesByHelpdeskId(helpdesk_Id);
            }
            if (!queryResult.Any())
            {
                return NotFound("Could not find any result");
            }
            var rank = queryResult.Select((textSearch, index) => new TextSearchRankDto()
            {
                Rank = index + 1,
                Count = textSearch.Count,
                Text = textSearch.SearchedText

            });
            return Ok(new TextSearchRankResponseDto(){rank = rank.ToList()});


        }

    }
}
