using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvGate.Api.Dtos;
using InvGate.Api.Infrastructure;
using InvGate.Api.Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace InvGate.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelpdesksController : ControllerBase
    {
        private readonly IInvGateApiService apiService;
        private readonly ITextSearchRankRepository textSearchRankRepository;
        private readonly ILogger<HelpdesksController> logger;

        public HelpdesksController(IInvGateApiService apiService, ITextSearchRankRepository textSearchRankRepository, ILogger<HelpdesksController> logger)
        {
            this.apiService = apiService;
            this.textSearchRankRepository = textSearchRankRepository;
            this.logger = logger;
        }
        
        /// <summary>
        ///  Devuelve la lista de los incidentes (ids o todo la data según el parámetro detailed) de la helpdesk cuyo id se pasó por parámetro.
        /// </summary>
        /// <remarks>
        /// SAMPLE:
        ///  GET http://localhost:5010/api/helpdesks?helpdesk_id=2&amp;text_to_search=disk&amp;detailed=1
        /// </remarks>
        /// <param name="helpdesk_id">Id de una mesa de ayuda a considerar</param>
        /// <param name="text_to_search">Un string a buscar en la description de los incidentes de la mesa de ayuda</param>
        /// <param name="detailed">si el parámetro está en cero, o no está; la respuesta debe tener sólo los ids de los incidentes que concuerden con la búsqueda, mientras que si el valor es uno se deben devolver los incidentes completos (es decir, todos sus campos). Cualquier otro valor, es considerado inválido.</param>
        /// <response code="200">Regresa la lista de incidentes solicitado</response>
        /// <response code="400">Cuando algun parametro tiene un valor invalido</response>     
        /// <response code="404">Cuando no se encuentran incidentes</response>     
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Produces("application/json")]
        public async Task<IActionResult> GetAsync([FromQuery] string helpdesk_id, [FromQuery] string text_to_search, [FromQuery] int? detailed)
        {
            if (string.IsNullOrWhiteSpace(helpdesk_id))
            {
                logger.LogError($"GetAsync. Bad request, invalid helpdesk_id");
                return BadRequest("helpdesk_id can't be empty");
            }
            if (detailed.HasValue && (detailed.Value > 1 || detailed.Value < 0))
            {
                logger.LogError($"GetAsync. Bad request, invalid detailed");
                return BadRequest("invalid value in detailed parameter");
            }

            var incidentsIds = await apiService.GetIncidentsByHelpdeskIdAsync(helpdesk_id);
            if (!incidentsIds.Any())
            {
                return NotFound("Incidents not found");
            }
            var detailedIncidents = await apiService.GetFullIncidentsByDescriptionAsync(incidentsIds, text_to_search);
            
            // Agregar esta busqueda valida a la tabla para contar las busquedas mas usadas
            if (!string.IsNullOrEmpty(text_to_search))
            {
                await textSearchRankRepository.Create(new Models.TextSearch() { HelpdeskId = helpdesk_id, SearchedText = text_to_search, Timestamp = DateTime.UtcNow.Ticks });
            }

            if (!detailedIncidents.Any())
            {
                return NotFound("Incidents containing text provided, not found ");
            }

            if (detailed.HasValue && detailed.Value == 1)
            {
                return Ok(MapToGetHelpdeskDetailedResponse(detailedIncidents));
            }
            return Ok(MapToGetHelpdeskShortResponse(detailedIncidents));
        }

        private object MapToGetHelpdeskShortResponse(IEnumerable<IncidentDto> detailedIncidents)
        {
            var response = new HelpdeskShortResponseDto() { incidentIds = new List<string>() };
            foreach (var incident in detailedIncidents)
            {
                response.incidentIds.Add(incident.id);
            }
            return response;
        }

        private object MapToGetHelpdeskDetailedResponse(IEnumerable<IncidentDto> detailedIncidents)
        {
            var response = new HelpdeskDetailedResponseDto() { incidents = new List<IncidentDto>() };
            foreach (var incident in detailedIncidents)
            {
                response.incidents.Add(incident);
            }
            return response;
        }
    }
}
