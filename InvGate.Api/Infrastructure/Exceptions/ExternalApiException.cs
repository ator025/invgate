using System;
using System.Runtime.Serialization;

namespace InvGate.Api.Infrastructure
{
    [Serializable]
    public class ExternalApiException : Exception
    {
        public ExternalApiException()
        {
        }

        public ExternalApiException(string message) : base(message)
        {
        }

        public ExternalApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExternalApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}