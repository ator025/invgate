﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using InvGate.Api.Dtos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace InvGate.Api.Infrastructure
{
    public class InvGateApiService : IInvGateApiService
    {
        private readonly HttpClient httpClient;
        private readonly ILogger<InvGateApiService> logger;

        public InvGateApiService(HttpMessageHandler httpMessageHandler, ILogger<InvGateApiService> logger)
        {
            httpClient = new HttpClient(httpMessageHandler);
            this.logger = logger;
        }
        public async Task<IEnumerable<string>> GetIncidentsByHelpdeskIdAsync(string id)
        {
            string responseAsString;
            dynamic dynResponse;
            try
            {

                var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, $"https://webdemo.cloud.invgate.net/api/v1/incidents.by.helpdesk?helpdesk_id={id}");
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", "YXBpdXNlcjo3NU53eHFQbUFBRW5lYldTNlB4Sjk0TUg=");
                var response = await httpClient.SendAsync(httpRequestMessage);

                response.EnsureSuccessStatusCode();

                responseAsString = await response.Content.ReadAsStringAsync();
                dynResponse = JsonConvert.DeserializeObject(responseAsString);
                
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"GetIncidentsByHelpdeskIdAsync. Error while processing Helpdesk: {id}");
                throw new ExternalApiException($"Helpdesk: {id}");
            }
            // revisando la respuesta en el response
            if (dynResponse.status == "OK")
            {
                return dynResponse.requestIds.ToObject<IEnumerable<string>>();
            }

            logger.LogError($"GetIncidentsByHelpdeskIdAsync. Unexpected Webdemo response: {responseAsString}");
            throw new ExternalApiException($"GetIncidentsByHelpdeskIdAsync. Status:{dynResponse.status}");


        }

        public async Task<IEnumerable<IncidentDto>> GetFullIncidentsByDescriptionAsync(IEnumerable<string> incidentIds, string textToSearch)
        {
            try
            {
                var result = new List<IncidentDto>();
                foreach (var incidentId in incidentIds)
                {

                    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, $"https://webdemo.cloud.invgate.net/api/v1/incident?id={incidentId}");
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", "YXBpdXNlcjo3NU53eHFQbUFBRW5lYldTNlB4Sjk0TUg=");
                    var response = await httpClient.SendAsync(httpRequestMessage);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseAsDto = await response.Content.ReadAsAsync<IncidentDto>();
                        if (string.IsNullOrEmpty(textToSearch) || responseAsDto.description.Contains(textToSearch))
                        {
                            result.Add(responseAsDto);
                        }
                    }
                    else
                    {
                        //logueamos y continuamos procesando los otros ids, si necesitamos control mas estricto, podemos lanzar una exception
                        logger.LogError($"GetFullIncidentsByDescriptionAsync. Webdemo api request has not a success status code, incident_Id: {incidentId}, response status code: {response.StatusCode}");
                    }

                }
                return result;
            }
            catch (Exception exception)
            {
                logger.LogError(exception, $"GetFullIncidentsByDescriptionAsync. Error while processing {string.Join(',', incidentIds)}");
                throw new ExternalApiException("GetFullIncidentsByDescriptionAsync");
            }
        }
    }
}