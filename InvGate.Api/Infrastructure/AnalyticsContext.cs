using InvGate.Api.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace InvGate.Api.Infrastructure
{
    public class AnalyticsContext : DbContext
    {
        public AnalyticsContext(DbContextOptions<AnalyticsContext> options)
            : base(options)
        {
        }

        public DbSet<TextSearch> TextSearchRanks { get; set; }
    }

}