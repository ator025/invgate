using System.Collections.Generic;
using System.Threading.Tasks;
using InvGate.Api.Dtos;

namespace InvGate.Api.Infrastructure
{
    public interface IInvGateApiService
    {
        Task<IEnumerable<string>> GetIncidentsByHelpdeskIdAsync(string id);
        Task<IEnumerable<IncidentDto>> GetFullIncidentsByDescriptionAsync(IEnumerable<string> incidentIds, string textToSearch);
    }
}