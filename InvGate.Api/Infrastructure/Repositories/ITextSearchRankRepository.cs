using System.Collections.Generic;
using System.Threading.Tasks;
using InvGate.Api.Models;

namespace InvGate.Api.Infrastructure.Repositories
{
    public interface ITextSearchRankRepository
    {
        Task<List<TextSearchCount>> GetTop5TextSearchesByHelpdeskId(string helpdeskId);
        Task<List<TextSearchCount>> GetGlobalTop5TextSearchesOrderByCountDescAsync();
        Task Create (TextSearch textSearch);
    }
}