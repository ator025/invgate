using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvGate.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace InvGate.Api.Infrastructure.Repositories
{
    public class TextSearchRankRepository : ITextSearchRankRepository
    {
        private readonly AnalyticsContext analyticsContext;

        public TextSearchRankRepository(AnalyticsContext analyticsContext)
        {
            this.analyticsContext = analyticsContext;
        }
        public async Task Create(TextSearch textSearch)
        {
            await analyticsContext.TextSearchRanks.AddAsync(textSearch);
            await analyticsContext.SaveChangesAsync();

        }

        public async Task<List<TextSearchCount>> GetGlobalTop5TextSearchesOrderByCountDescAsync()
        {
            return await analyticsContext.TextSearchRanks.GroupBy(text => text.SearchedText)
                                                          .Select(group => new TextSearchCount()
                                                          {
                                                              SearchedText = group.Key,
                                                              Count = group.Count()
                                                          })
                                                         .OrderByDescending(c => c.Count)
                                                         .Take(5)
                                                         .ToListAsync();
        }

        public async Task<List<TextSearchCount>> GetTop5TextSearchesByHelpdeskId(string helpdeskId)
        {
            return await analyticsContext.TextSearchRanks .Where(tSearch => tSearch.HelpdeskId == helpdeskId )
                                                          .GroupBy(text => text.SearchedText)
                                                          .Select(group => new TextSearchCount()
                                                          {
                                                              SearchedText = group.Key,
                                                              Count = group.Count()
                                                          })
                                                         .OrderByDescending(c => c.Count)
                                                         .Take(5)
                                                         .ToListAsync();
        }
    }
}