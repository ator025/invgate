using System.Collections.Generic;
namespace InvGate.Api.Dtos
{
    public class IncidentDto
    {
        public string id { get; set; }
        public string title { get; set; }
        public string category_id { get; set; }
        public string description { get; set; }
        public string priority_id { get; set; }
        public string user_id { get; set; }
        public string creator_id { get; set; }
        public string assigned_id { get; set; }
        public string assigned_group_id { get; set; }
        public string date_ocurred { get; set; }
        public string source_id { get; set; }
        public string status_id { get; set; }
        public string type_id { get; set; }
        public long? created_at { get; set; }
        public long? last_update { get; set; }
        public string process_id { get; set; }
        public long? solved_at { get; set; }
        public long? closed_at { get; set; }
        public string closed_reason { get; set; }
        public object data_cleaned { get; set; }
        public List<object> attachments { get; set; }
    }
}
