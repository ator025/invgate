using System.Collections.Generic;

namespace InvGate.Api.Dtos
{
    public class HelpdeskShortResponseDto
    {
        public List<string> incidentIds { get; set; }
    }
}