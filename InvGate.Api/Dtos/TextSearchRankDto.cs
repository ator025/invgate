namespace InvGate.Api.Dtos
{
    public class TextSearchRankDto
    {
        public int Rank { get; set; }
        public string Text { get; set; }
        public long Count { get; set; }
    }
}