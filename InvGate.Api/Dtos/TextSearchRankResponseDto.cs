using System.Collections.Generic;

namespace InvGate.Api.Dtos
{
    public class TextSearchRankResponseDto
    {
        public List<TextSearchRankDto> rank { get; set; }
    }
}