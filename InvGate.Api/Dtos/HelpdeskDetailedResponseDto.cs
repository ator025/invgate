using System.Collections.Generic;

namespace InvGate.Api.Dtos
{
    public class HelpdeskDetailedResponseDto
    {
        public List<IncidentDto> incidents { get; set; }
    }
}